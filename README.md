# learn-workspaces-gitpod

## PostgreSQL

The server auto-starts once a new terminal is opened in Gitpod.

Utils:

```
pg_start
pg_stop

pg_ctl status
```


More details in [this blog post](https://www.gitpod.io/blog/gitpodify/#postgresql).
